export default function SearchInput({query, setQuery}) {
    return(
        <form className="new-item-form">
            <div className="form-row">
                <label htmlFor="item">Search</label>
                <input 
                type="search" 
                id="item" 
                placeholder="Search..." 
                value={query}
                onChange={e => setQuery(e.target.value)}
                />
            </div>
        </form>
    )
}